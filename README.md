# Cloud Infrastructure Code Challenge

In lieu of a traditional coding test, this challenge attempts to mirror the common problems that Cloud Engineer's solve. 

## Problem 1 (Python)
Z is Acme Corps first engineer. Acme has developed a trans dimensional warp disruptor, however its prone to overheating! Z has been asked to write an API that when queried via the route (/warp/temp/get) returns the current outside air temperature. If the air temp is higher than 50F log the event "too hot" otherwise "too cold". With each log statement also include the air temp value and a timestamp. 

## Problem 2 (AWS CodeBuild and ECR)
Now that we have our service, its time to build it! 

1. Containerize the service via a Dockerfile 
2. Create a Continuous Integration (CI) process that builds your Dockerfile and pushes it to a registry. 
3. Scan your container for vulnerabilities.

## Problem 3 (AWS CodeBuild and EKS)
Now that we have our container built, its time to deploy! 

1. Create a Continuous Deployment (CD) process that deploys your service to a cloud of your choice.
2. Expose your service to the outside world. 
3. Don't forget about security! Be prepared to explain the security implications of how you have exposed the service. 
4. Bonus points if you dynamically scale your service and do rolling updates. 

## Problem 4 (AWS Cloudwatch)
Now that our service is deployed, its time to monitor it!

1. Using a monitoring tool of your choice, collect the logs and metrics from your service.
2. Create an alert if the temperature is too hot. 
3. Display a graph of how many times the service has been called. 

## The Interview
You will have 48 hours to complete the challenge. Once you have completed commit all your code and any configuration files to a GIT repo and share with your interviewer(s). During your interview, be prepared to share your screen and demo a working copy of your deployed service. You will be asked to walk through each of the problems and explain your solutions and choice of technologies. 

## Tips
 * This is your chance to show the interviewer(s) your experience and skills!
 * Be prepared to talk in depth about any decisions you make. Think of how you would explain your solution to a coworker. 
 * You may use different technologies then suggested, but be prepared to explain your reasoning. The technologies listed are some of what we currently use. 
 * Feel free to expand upon the problems if you would like to highlight specific skills. 
 * Bonus points if you can compare and contrast different technologies or solutions!
 * If you have any questions or need clarification please reach out to your interviewer(s). 
